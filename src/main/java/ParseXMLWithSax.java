import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ParseXMLWithSax {

    public static void main(String[] args) {
        boolean nameAttrFound = false;
        boolean valueAttrFound = false;
        boolean langCdAttrFound = false;
        boolean corectLangDescrCodeFound = false;
        String langDescr = "";

        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader =
                    factory.createXMLEventReader(new FileReader("src/main/resources/krdm_message.xml"));

            while(eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                switch(event.getEventType()) {

                    case XMLStreamConstants.START_ELEMENT:
                        StartElement startElement = event.asStartElement();
                        String qName = startElement.getName().getLocalPart();

                        if (qName.equalsIgnoreCase("name")) {
                            nameAttrFound = true;
                        } else if (qName.equalsIgnoreCase("value")) {
                            valueAttrFound = true;
                        }
                        break;

                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();
                        if(corectLangDescrCodeFound && nameAttrFound) {
                            if (characters.getData().equals("Desc")) {
                                System.out.println("Descr: " + langDescr);
                            }
                            corectLangDescrCodeFound = false;
                            langCdAttrFound = false;
                        }
                        if(langCdAttrFound) {
                            if (characters.getData().equals("00")) {
                                corectLangDescrCodeFound = true;
                            }
                        }
                        if(nameAttrFound) {
                            if (characters.getData().equals("LangCd")) {
                                langCdAttrFound = true;
                            }
                            nameAttrFound = false;
                        }
                        if(valueAttrFound) {
                            langDescr = characters.getData();
                            valueAttrFound = false;
                        }
                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        EndElement endElement = event.asEndElement();

                        if(endElement.getName().getLocalPart().equalsIgnoreCase("student")) {
                            System.out.println("End Element : student");
                            System.out.println();
                        }
                        break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

}
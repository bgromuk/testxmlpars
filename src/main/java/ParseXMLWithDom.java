import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

public class ParseXMLWithDom {

    public static void main(String argv[]) {

        try {

            Document doc = getDocumentFromXml();

            NodeList nList = doc.getElementsByTagName("name");

            for (int nameAttsInd = 0; nameAttsInd < nList.getLength(); nameAttsInd++) {
                Node nNode = nList.item(nameAttsInd);

                if (nNode.getParentNode().getNodeName().equals("Attribute")) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getTextContent();

                    if (name.equals("LangCd")) {

                        Node parentNode = eElement.getParentNode();
                        Element langAttributeParentNode = (Element) parentNode;

                        String langCodeValue = langAttributeParentNode.getElementsByTagName("value").item(0).getTextContent();

                        if (langCodeValue.equals("00")) {
                            NodeList engLangAttribute = langAttributeParentNode.getParentNode().getChildNodes();
                            NodeList langAttNames = ((Element) engLangAttribute).getElementsByTagName("name");

                            for (int langAttInd = 0; langAttInd < langAttNames.getLength(); langAttInd++) {
                                Node langItem = langAttNames.item(langAttInd);
                                String langDescrType = langItem.getTextContent();

                                if (langDescrType.equals("Desc")) {
                                    NodeList descrValue = ((Element) langItem.getParentNode()).getElementsByTagName("value");
                                    System.out.println(descrValue.item(0).getTextContent());
                                    break;
                                }
                            }

                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Document getDocumentFromXml() throws ParserConfigurationException, SAXException, IOException {
        File fXmlFile = new File("src/main/resources/krdm_message.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);

        doc.getDocumentElement().normalize();
        return doc;
    }

}